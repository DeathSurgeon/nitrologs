package parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Parser {

	static private String inputFileDirectory = "/home/ubuntu/sigview-api/";
	static private String fileName = "application-log-";
	static private String outputDirectory = "/home/ubuntu/sigview-api/log-parser/output/";
	

	public static void main(String a[]) {

		inputFileDirectory = a[0];
		outputDirectory = a[1];

		Map<String, String> querymap = new HashMap<>();
		Map<String, String> elapsedTimeMap = new HashMap<>();

		System.out.println(a[0] + " " + a[1]);
		try {
			if(a.length > 2) {
				fileName = a[2] ;
			}
			else {
				evaluateFileName();
			}

			System.out.println(fileName);

			File file = new File(inputFileDirectory + fileName);
	
			if (file.isFile()) {			
				commandExecutor("gunzip " + fileName, inputFileDirectory);
				System.out.println("gunzip command executed ");
				BufferedReader reader = new BufferedReader(new FileReader(inputFileDirectory + fileName.substring(0,fileName.length() - 3)));
				String line = "";
				while ((line = reader.readLine()) != null) {
					if (line.contains("dimensions:")) {
						if (line.contains("requestType")) {
							if (line.split("requestType")[1].replaceAll("^:+", "").trim().compareTo("chartType") != 0)
								continue;
						} 
						else continue;
						
						String temp = line.split("organization")[0].replaceAll("^:+", "").trim();
						String dateString = temp.split("INFO")[0].replaceAll("\\[", "").trim();
						String userString = temp.split("user")[1].replaceAll(",", "").replaceAll(":", "")
								.replaceAll("0000", "").trim();
						String jsonFiltersString = line.split("filters")[1].replaceAll("^:+", "").trim();
						String jsonColumnString = line.split("dimensions")[1].replaceAll("^:+", "").trim();

						String groupBy = parseJsonArray(jsonColumnString, "columnID");
						String filterList = parseJsonArray(jsonFiltersString, "filterID");

						String requestId = line.split("requestId")[1].split(",")[0].replaceAll("^:+", "").trim();
						String[] parse = line.split(" ") ;
						String timeOfQuery = parse[0] + " " + parse[1] ;
						Long startTime = Long.parseLong(find(line.split(","), "queryStartTime"));
						Long endTime = Long.parseLong(find(line.split(","), "queryEndTime"));

						if (groupBy.isEmpty())
							groupBy = "NULL";
						String completeLog = timeOfQuery + "\t" + userString + "\t" + groupBy + "\t" + filterList + "\t"
								+ timeFunction(endTime - startTime) + "\t";

						querymap.put(requestId, completeLog);

					}

					if (line.contains("elapsed")) {
						String elapsed = find(line.split(","), "elapsed") ;
						String status = line.split("status")[1].split(",")[0].replaceAll(":", "").trim();
						
						int elapsedTime = Integer.parseInt(elapsed) ;
						String requestId = line.split("requestId")[1].split(",")[0].replaceAll("^:+", "").trim() ;
						
						elapsedTimeMap.put(requestId, elapsed + "\t" + status) ;
					}

				}
			}

			FileWriter writer = new FileWriter(new File(outputDirectory + "analysis.txt"),
					true);
			writer.write("Request Id" + "\t" + "Time of Query" + "\t" + "User" + "\t" + "Group By" + "\t" + "Filter" + "\t" + "Query Duration" + "\t" + "Elapsed Time" + "\t" + "Status" + "\n") ;
			
			for(String requestID : querymap.keySet()) {	
				//writer.write(requestID + "\t" + querymap.get(requestID).toString() + elapsedTimeMap.get(requestID).toString() + "\n") ;
				if(elapsedTimeMap.get(requestID) != null)
					writer.write(requestID + "\t" + querymap.get(requestID).toString() + elapsedTimeMap.get(requestID).toString() + "\n") ;
				else 
					writer.write(requestID + "\t" + querymap.get(requestID).toString() + "\n") ;
				
			}
			
			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static String timeFunction(long a) {

		StringBuilder s = new StringBuilder();
		a /= 1000;
		int day = (int) TimeUnit.SECONDS.toDays(a);
		long hours = TimeUnit.SECONDS.toHours(a) - (day * 24);
		long minute = TimeUnit.SECONDS.toMinutes(a) - (TimeUnit.SECONDS.toHours(a) * 60);
		long second = TimeUnit.SECONDS.toSeconds(a) - (TimeUnit.SECONDS.toMinutes(a) * 60);
		return day + " days : " + hours + " hrs : " + minute + " min : " + second + " sec ";

	}

	private static void evaluateFileName() {
		String yesterdayDate = getYesterdayDateString();
		fileName += yesterdayDate + ".gz";
	}

	public static String parseJsonArray(String jsonString, String tag) {
		String finalValue = "";
		try {
			JSONArray jsonarray = new JSONArray(jsonString);
			String columnIDs = "";
			List<String> listOfFilters = new ArrayList<String>();
			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				if (tag.equals("filterID")) {
					listOfFilters.add(jsonobject.getString(tag));
				} else {
					columnIDs = jsonobject.getString(tag);
					// System.out.println("ColumnID " + columnIDs);
				}

			}

			Collections.sort(listOfFilters);
			String temp = "";

			int sizeOfList = listOfFilters.size();
			for (int i = 0; i < sizeOfList - 1; i++) {
				temp += listOfFilters.get(i) + ",";
			}
			if (sizeOfList - 1 >= 0)
				temp += listOfFilters.get(sizeOfList - 1);

			if (tag.equals("filterID")) {
				// System.out.println("Filters = " + temp);
				finalValue = temp;
			} else
				finalValue = columnIDs;

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return finalValue;

	}

	private static String find(String[] split, String str) {

		for (String s : split) {
			if (s.contains(str)) {
				return s.split(":")[1].trim().replaceAll("[a-z]*", "").trim();
			}
		}

		return null;
	}

	public static String getYesterdayDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	private static String commandExecutor(String command, String home) {
		int prcount = 0;
		String output = "";
		int errorCode = 0;

		try {
			ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);
			processBuilder.directory(new File(home));
			Process p = processBuilder.start();
			errorCode = p.waitFor();

			BufferedReader buf = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null) {
				output = line;
			}
			p.destroy();
		} catch (Exception e) {
			System.out.println("Error while executing the command");

			e.printStackTrace();
		}
		if (errorCode == 0)
			return output;
		else {
			return "error";
		}

	}
}
